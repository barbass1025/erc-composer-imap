<?php

namespace Erc\Email;

/**
 * Класс работы с почтой по Imap
 */
class Imap {
	protected $options = [];
	protected $inbox = null;

	public function __construct($options = []) {
		$this->options = $options;
	}

	public function connect() {
		$this->inbox = \imap_open($this->options['connection'], $this->options['username'], $this->options['password']);
	}

	/**
	 * Получение писем
	 * @param string $search_option Критерий поиска
	 * @return mixed (false | array)
	 */
	function get($search_option = 'UNSEEN', $options = SE_FREE, $charset = "UTF-8") {
		if (!$this->inbox) {
			$this->connect();
		}

		$error = \imap_last_error();
		if ($error !== false) {
			throw new \Exception($error);
		}

		// search and get unseen emails, function will return email ids
		$search_option = (!empty($search_option)) ? $search_option : 'UNSEEN';

		$email_list = \imap_search($this->inbox, $search_option, $options, $charset);

		if (!$email_list) {
			return false;
		}

		$output = [];
		foreach($email_list as $mail) {
			$headerInfo = \imap_headerinfo($this->inbox, $mail);

			$letter['subject'] = \imap_utf8($headerInfo->subject);
			$letter['to'] = \imap_utf8($headerInfo->to[0]->mailbox.'@'.$headerInfo->to[0]->host);
			$letter['date'] = \imap_utf8($headerInfo->date);
			$letter['from'] = \imap_utf8($headerInfo->from[0]->mailbox.'@'.$headerInfo->from[0]->host);
			$letter['text'] = '';
			$letter['files'] = [];

			$emailStructure = \imap_fetchstructure($this->inbox, $mail);
			if (isset($emailStructure->parts)) {
				foreach($emailStructure->parts as $subsection => $part) {
					$section = "" . ($subsection + 1);
					if (isset($part->disposition)) {
						if (in_array(strtolower($part->disposition), array('attachment','inline'))) {
							$filename = $headerInfo->fromaddress.$section;
							if (isset($part->parameters)) {
								foreach($part->parameters as $parameter) {
									if ($parameter->attribute == 'name') {
										$filename = sys_get_temp_dir().'/'.\imap_utf8($parameter->value);
									}
								}
							}
							array_push($letter['files'], $filename);
							//FT_PEEK - оставлять сообщения непрочитанными после получения
							file_put_contents($filename, base64_decode(\imap_fetchbody($this->inbox, $mail, $section, FT_PEEK)));
						}
					} else {
						$letter['text'] .= \imap_fetchbody($this->inbox, $mail, $section, FT_PEEK).PHP_EOL;
					}
				}
			} else {
				$letter['text'] = \imap_utf8(\imap_body($this->inbox, $mail, FT_PEEK));
			}
			array_push($output, $letter);
		}


		return $output;
	}

	public function close() {
		if ($this->inbox) {
			\imap_close($this->inbox);
			$this->inbox = null;
		}
	}

	public function __destruct() {
		$this->close();
	}

}
